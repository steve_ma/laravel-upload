<?php
namespace Stevema\Upload;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rules\File;
class UploadValidate extends FormRequest
{
    public function rules(): array
    {
        return [
            'file' => [
                'required',
//                'mimes:jpg,jpeg,gif,png,bmp,tga,tif,pdf,psd,avi,mp4,mp3,wmv,mpg,mpeg,mov,rm,ram,swf,flv,pem,ico',
//                'mimetypes:video/avi,video/mpeg,video/quicktime',
                File::types([
                    'jpg','jpeg','gif','png','bmp','tga','tif','pdf','psd',
                    'avi','mp4','mp3','wmv','mpg','mpeg','mov','rm','ram','swf','flv','pem','ico'
                ])
                    ->min(1)
                    ->max(50 * 1024),
//            File::image()
//                ->min(1024)
//                ->max(12 * 1024)
//                ->dimensions(Rule::dimensions()->maxWidth(1000)->maxHeight(500)),
            ],
        ];
    }

    public function messages()
    {
        return [
            'file.mimes' => '该文件类型不允许上传',
        ];
    }

    /**
     * 重写报错部分-适应API JSON下发的需求
     */
    protected function failedValidation(Validator $validator)
    {
        throw (new HttpResponseException(response()->json($validator->errors()->messages(), 400)));
    }
}
