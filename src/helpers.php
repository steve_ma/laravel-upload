<?php
use Stevema\Upload\UploadManage;
use Stevema\Upload\UploadException;
if (!function_exists('upload')) {
    function upload(): UploadManage
    {
        if(function_exists('app')) {
            return app('upload');
        }
        $conf = ['default'=> 'local', 'local'=>[]];
        return new UploadManage($conf);
    }
}
if (!function_exists('upload_file')) {
    function upload_file($input_name='file', $save_dir=null, $validate=null): UploadManage
    {
        if(empty($save_dir)) $save_dir = 'uploads/'.date("Y/md");
        $upload = app('upload');

        # 可以自己指定文件验证 - 也可以不指定 走默认
        if($validate) {
            $upload->setUploadValidate($validate);
        }
        # 写入文件 - 上传的文件  file是上传文件的字段名
        $upload->setUploadFile($input_name);
        if (!$upload->upload($save_dir)) throw new UploadException($upload->getError());
        $fileName = $upload->getFileName();
        $fileInfo = $upload->getFileInfo();
        // 处理文件名称
        if (strlen($fileInfo['name']) > 128) {
            $file_name = substr($fileInfo['name'], 0, 123);
            $file_end = substr($fileInfo['name'], strlen($fileInfo['name'])-5, strlen($fileInfo['name']));
            $fileInfo['name'] = $file_name.$file_end;
        }
        // 具体路径
        $uri = $save_dir . '/' . str_replace("\\","/", $fileName);
        $uri = $upload->getFileUrl($uri);

        return [
            'uri'         => $uri,
            'size'        => $fileInfo['size'],
            'name'        => $fileInfo['name'],
            'mime'        => $fileInfo['mime'],
            'ext'         => $fileInfo['ext'],
        ];
    }
}
