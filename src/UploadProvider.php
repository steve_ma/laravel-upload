<?php

namespace Stevema\Upload;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class UploadProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->singleton('upload', function(Application $app){
            return new UploadManage($app['config']->get('upload'));
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        // 发布配置文件
        $this->initPublishes();
        // 合并配置
        $this->mergeConfigFrom(
            realpath(__DIR__.'/Config/config.php'), 'upload'
        );
    }
    protected function initPublishes(){
        // 发布配置文件
        if ($this->app->runningInConsole()) {
            $this->publishes([
                realpath(__DIR__.'/Config/config.php') => config_path('upload.php'),
//          $from => $to
//	        __DIR__.'.../xxx.php' => config_path('xxx.php'), //配置文件
//	        __DIR__.'.../migrations' => database_path('migrations'), //迁移文件
//	        __DIR__.'.../views' => resource_path('views/vendor'), //视图文件
//	        __DIR__.'.../translations' => resource_path('lang/vendor'), //翻译文件

            ],'upload');
        }
    }
}
