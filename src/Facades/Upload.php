<?php
namespace Stevema\Facades;

use Stevema\Upload\UploadManage;
use Illuminate\Support\Facades\Facade;
class Upload extends Facade
{
    /**
     * Get the name of the class registered in the Application container.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'upload';
//        return UploadManage::class;
    }
}
